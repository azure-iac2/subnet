# Create subnet
resource "azurerm_subnet" "subnet" {
  name                 = "${var.business_divsion}-${var.environment}-subnet"
  resource_group_name  = data.azurerm_resource_group.rg.name
  virtual_network_name = data.azurerm_virtual_network.vnet.name
  address_prefixes     = ["${var.sn_address_space}"]
}